package base;


import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author Julio
 */
public interface ConsultaMail extends Remote {
    public boolean consulta(String name) throws RemoteException;
}
