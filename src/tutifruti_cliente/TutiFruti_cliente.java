/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tutifruti_cliente;

import tutifruti.RmiServerIntf;
import java.rmi.RMISecurityManager;
import base.ConsultaMail;
import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 *
 * @author Lorenzo
 */
public class TutiFruti_cliente {
    
    private static boolean GUI = false;
    private static boolean Local = false;
    private static String PUERTO = "1099";
    private static Registry registry;
    private static String user = "lorenzom";
    private static String password = "armazon5";
//    private static String user = "usu1";
//    private static String password = "cla1";
//    private static String IPREMOTO = "//grid2.frm.utn.edu.ar:"+PUERTO+"/RmiServer";
    private static String IPREMOTO = "//jmonetti.ddns.net:"+PUERTO+"/RmiServer";
    private RmiServerIntf obj = null;
    private String categoria;
    private String letra;
    public TutiFruti_cliente() {
        comunicar();
    }
    
    //pais-frutaverdura-ciudad-cosa-deporte-nombremasculino-nombrefemenino-vegetal-acciones-cantantes

    private void comunicar(){
        while(obj==null){
            System.out.println("conectando...");
            try{
                if(System.getSecurityManager()==null){
                    System.setProperty("java.security.policy","java.policy");
                    System.setSecurityManager(new RMISecurityManager());
                }
                
                obj = (RmiServerIntf) Naming.lookup(IPREMOTO);
                System.out.println("Se comenzará la comunicación");
                System.out.println(obj.comenzarComunicacion(user, password));
            }                
            catch (Exception e) {
                String error = e.getMessage();
                e.printStackTrace();
            }
        }
    }
    
    
    public void getCategoria(){
        System.out.println("categoria...");
           try{
                categoria = obj.getCategoria(user, password);
                System.out.println(categoria);
           }
           catch (Exception e) {
                String error = e.getMessage();
                e.printStackTrace();
            }
        
    }
    public void setLetra(String palabraSet){
       System.out.println("seteando palabra...");
           try{
                System.out.println(obj.setPalabra(palabraSet,user, password));
            }
           catch (Exception e) {
                String error = e.getMessage();
                e.printStackTrace();
            } 
    }
    
    public void getLetra(){
        System.out.println("letra...");
           try{
                letra = obj.getLetra(user, password);
                System.out.println(letra);
           }
           catch (Exception e) {
                String error = e.getMessage();
                e.printStackTrace();
            }
    }
    public void getMisPuntos(String palabraSet){
        System.out.println("puntos...");
           try{
                System.out.println(obj.getMisPuntos(palabraSet,user));
           }
           catch (Exception e) {
                String error = e.getMessage();
                e.printStackTrace();
            }
    }
    public void getGanador(String palabraSet){
         System.out.println("ganador...");
           try{
                System.out.println(obj.getGanador(palabraSet,user));
           }
           catch (Exception e) {
                String error = e.getMessage();
                e.printStackTrace();
            }
    }
    public String leerTXT() {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        String linea="";
        String lineaDevolver="";
        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new File ("E:\\EJ_RMI_CONSULTA_EXISTENCIA_MAIL_CLIENTE\\tutifruti\\"+categoria+".txt");
            fr = new FileReader (archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            boolean listo=false;
            while((linea=br.readLine())!=null && !listo){
                if(String.valueOf(linea.charAt(0)).toLowerCase().equals(letra.toLowerCase())){
                    lineaDevolver=linea;
                    listo=true;
                }
            }
        }
        catch(Exception e){
           e.printStackTrace();
        }finally{
           // En el finally cerramos el fichero, para asegurarnos
           // que se cierra tanto si todo va bien como si salta 
           // una excepcion.
           try{                    
              if( null != fr ){   
                 fr.close();    
              }                  
           }catch (Exception e2){ 
              e2.printStackTrace();
           }
        }
        return lineaDevolver;
   }
    
    public void LlenarDiccionario()
    {
        FileWriter fichero = null;
        PrintWriter pw = null;
        System.out.println("llenando diccionario...");
        try{
            String cantPalabras = obj.getCantidadPalabra(user, password,categoria);
            String ruta = categoria;
            obj.setLugarPalabra(user, password,categoria,0);
            for(int i=0;i<Integer.parseInt(cantPalabras);i++){
                String palabraDicc = obj.getPalabra(user, password,categoria);
                try
                {
                    fichero = new FileWriter("E:\\EJ_RMI_CONSULTA_EXISTENCIA_MAIL_CLIENTE\\tutifruti\\"+ruta+".txt",true);
                    pw = new PrintWriter(fichero);

                    File archivo = null;
                    archivo = new File ("E:\\EJ_RMI_CONSULTA_EXISTENCIA_MAIL_CLIENTE\\tutifruti\\"+ruta+".txt");
                    FileReader fr = null;
                    BufferedReader br = null;
                    fr = new FileReader (archivo);
                    br = new BufferedReader(fr);

                    String linea;    
                    List<String> listaPalabras = new ArrayList<String>();
                    while((linea=br.readLine())!=null){
                         listaPalabras.add(linea);
                    }
                    BufferedWriter output = new BufferedWriter(fichero);

                    if(!listaPalabras.contains(palabraDicc)){
                        fichero.write(palabraDicc+"\r\n");
                        System.out.println("palabra agregada...");
                    }                       


                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                   try {
                   // Nuevamente aprovechamos el finally para 
                   // asegurarnos que se cierra el fichero.
                   if (null != fichero)
                      fichero.close();
                   } catch (Exception e2) {
                      e2.printStackTrace();
                   }
                }
            }
            System.out.println("fichero cerrado...");
        }
        catch (Exception e) {
            String error = e.getMessage();
            e.printStackTrace();
        }
    }
}